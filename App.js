import React, {Component} from "react";
import { SafeAreaView, StyleSheet, TextInput, Button, Text } from "react-native";


class App extends Component {
  state = {
    firstname:"",
    lastname:"",
    alamat:""
  }
  firstnameTextChange = (inputText) =>{
    this.setState({firstname : inputText})
  }

  lastnameTextChange = (inputText) =>{
    this.setState({lastname : inputText})
  }
  alamatTextChange = (inputText) =>{
    this.setState({alamat : inputText})
  }

  hasil = () => {
    alert ("halo  " + this.state.firstname + this.state.lastname + " alamat mu : "+this.state.alamat)
  }
  render() {
    return (
      <SafeAreaView>
        <TextInput
          style={styles.input}
          onChangeText={this.firstnameTextChange}
          placeholder = "masukkan nama awal"
        />
        <TextInput
          style={styles.input}
          onChangeText={this.lastnameTextChange}
          placeholder = "masukkan nama akhir"
        />
        <TextInput
          style={styles.input}
          onChangeText={this.alamatTextChange}
          placeholder = "masukkan alamat"
        />
        <Button styles={styles.tombol}
          title="tekan saya"
          onPress={this.hasil}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
  tombol: {
    backgroundColor:'red'
  }
});

export default App;